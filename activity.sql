

SELECT * 
FROM artists
WHERE name LIKE "%D%";


SELECT *
FROM songs
WHERE length < 230;


SELECT albums.name AS album_name , songs.title AS song_name, songs.length AS song_length
FROM albums JOIN songs
ON albums.id = songs.album_id;



SELECT *
FROM artists JOIN albums
ON artists.id = albums.artist_id
WHERE albums.name LIKE "%A%";


SELECT *
FROM albums
ORDER BY name DESC
LIMIT 4;



SELECT * 
FROM albums JOIN songs
ON albums.id = songs.album_id
ORDER BY albums.name DESC, songs.title ASC;

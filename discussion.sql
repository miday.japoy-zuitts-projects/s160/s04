


insert into artists (name)
values("Taylor Swift");


insert into albums (name, year, artist_id)
values("Fearless" , 2008, 3),
("Rend" , 2012, 3),
("A Star Is Born" , 2018, 4),
("Born This Way" , 2011, 4),
("Purpose", 2015, 5),
("Believe", 2012, 5),
("Dangerous Woman", 2016, 6),
("Thank U, Next", 2019, 6),
("24K Magic", 2016, 7),
("Earth to Mars", 2011, 7);


insert into songs (title, length, genre, album_id)
values("Fearless", 246, "Pop Rock", 3),
("Love Story", 213, "Country Pop", 3),
("State of Grace", 313, "Alternative Rock", 4),
("Red", 204, "Country", 4),
("Black Eyes", 181, "Rock n Roll", 5),
("Shallow", 201, "Country, Rock, Folk Rock", 5),
("Born This Way", 252, "Electropop", 6),
("Sorry", 192, "DPH", 7),
("Boyfriend", 251, "Pop", 8),
("Into You", 242, "EDM House", 9),
("Thank You, Next", 196, "Pop, RNB", 10),
("24K Magic", 207, "Funk, Disco, RnB", 11),
("Lost", 193, "Pop", 12);